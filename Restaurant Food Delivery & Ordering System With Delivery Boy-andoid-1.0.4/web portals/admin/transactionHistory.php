<?php require_once("Header.php"); ?>



<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<?php require_once('left-sidebar.php'); ?>			
	<div class="main-content">
	<?php require_once('topbar.php'); ?>		
	
	<div class="row">
        <div class="col-md-12">
        
        <div class="pull-left"><h2 class="toast-title">View All Transaction History</h2></div>
       <!-- <div class="pull-right"><a style="position: relative; top: 10px;" href='javascript:;' onClick='addtax()' class='btn btn-default'>Add Currency</a></div>-->
        <div class="clearfix"></div>
        <br>
         <table class="display nowrap table table-hover table-striped table-bordered" id="table-1">
            <thead>
                <tr>
                    <th><b>ID</b></th>
                    <th><b>Date</b></th>
                    <th><b>Time</b></th>
                    <th><b>Type</b></th>
                    <th><b>Amount(RM)</b></th>
					<th><b>Description</b></th>
					<th><b>To whom</b></th>
                    <th><b>Action</b></th>
                </tr>
            </thead>
            <tbody>
                <td>5e112aP4</td>
                <td>11/06/2021</td>
                <td>12:32 PM</td>
                <td>Royalty fee</td>
                <td><i class="fas fa-minus" style="color:red; margin-right: 10px;"></i>200</td>
                <td>Pays royalty to MD company</td>
                <td>Mahiran Digital</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
                <?php 
				 
                /*$url = $baseurl."/showRestaurants";
                $params = "";
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                $json_data = json_decode($result, true);
                //echo var_dump($json_data);
                //$i=0;
                foreach($json_data['msg'] as $data) {
                    //var_dump($data);
                     if(!empty($data['Restaurant']['id'])) {
                        echo "<tr>
						

                            <td>".$data['Restaurant']['id']."</td>
                            <td>".$data['Restaurant']['name']."</td>
							 <td>".$data['UserInfo']['first_name']." ".$data['UserInfo']['last_name']."</td>
							 <td>".$data['UserInfo']['phone']."</td>
							 
                           
                           <td>
						   <a href='javascript:void(0);' data-href='restaurantscoupons.php?userid=".$data['UserInfo']['user_id']."' class='viewrirestcoupons btn btn-default btn-sm'>View all Coupons</a>
						 	
							<a href='javascript:;' onClick='addCoupan(".$data['Restaurant']['id'].")' class='viewrirestcoupons btn btn-default btn-sm'>Add Coupons</a>
						   </td>
                           
                              
                        </tr>";
                    }
                    //$i++;
                }

                curl_close($ch);*/
                ?>
            </tbody>
            <tbody>
                <td>5f912aX2</td>
                <td>09/06/2021</td>
                <td>13:20 PM</td>
                <td>Commission</td>
                <td><i class="fas fa-minus" style="color:red; margin-right: 10px;"></i>50</td>
                <td>Pays commission to runner</td>
                <td>Ahmad Bob</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
            </tbody>
            <tbody>
                <td>2l012aM2</td>
                <td>05/06/2021</td>
                <td>09:52 AM</td>
                <td>Wallet</td>
                <td><i class="fas fa-plus" style="color:green; margin-right: 10px;"></i>1000</td>
                <td>Topup wallet RM100</td>
                <td>Foodium System</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
            </tbody>
            <tbody>
                <td>12e612sK8</td>
                <td>04/06/2021</td>
                <td>10:15 AM</td>
                <td>Commission</td>
                <td><i class="fas fa-minus" style="color:red; margin-right: 10px;"></i>55</td>
                <td>Pays commission to runner</td>
                <td>Ahmad KC</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
            </tbody>
            <tbody>
                <td>k12Afa33a</td>
                <td>04/06/2021</td>
                <td>10:05 PM</td>
                <td>Commission</td>
                <td><i class="fas fa-minus" style="color:red; margin-right: 10px;"></i>40</td>
                <td>Pays commission to runner</td>
                <td>Ahmad Mohs</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
            </tbody>
            <tbody>
                <td>O12Eaf54P</td>
                <td>03/06/2021</td>
                <td>14:22 PM</td>
                <td>Commission</td>
                <td><i class="fas fa-minus" style="color:red; margin-right: 10px;"></i>55</td>
                <td>Pays commission to runner</td>
                <td>Ahmad Hisy</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
            </tbody>
            <tbody>
                <td>0asisje86</td>
                <td>01/06/2021</td>
                <td>14:22 PM</td>
                <td>Wallet</td>
                <td><i class="fas fa-plus" style="color:green; margin-right: 10px;"></i>500</td>
                <td>Topup wallet</td>
                <td>Foodium System</td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>View detail</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Delete</a></td>
            </tbody>
            
        </table>
        
        <script type="text/javascript">
        
             <?php

                  if($_SESSION['role']=="0")
                  {
                     ?>
                          $('#table-1').DataTable({
                              dom: 'Bfrtip',
                              buttons: [
                                  'copy', 'csv', 'excel', 'pdf', 'print'
                              ]
                          });
                     <?php 
                  }
              ?>
        </script>
         
            
        </div>
    </div>    


<script type="text/javascript">
function addCoupan(data) {
   
   jQuery('#modal-7').modal('show', {backdrop: 'static'});
    document.getElementById("addCoupan").innerHTML="loading...";
    var xmlhttp;
    if(window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      xmlhttp.onreadystatechange=function()
      {
        if(xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           // alert(xmlhttp.responseText);
            document.getElementById("addCoupan").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","ajex-events.php?addCoupan=ok&resID="+data);
    xmlhttp.send();
	
}




jQuery(document).ready(function(){ 
	 $('.viewrirestcoupons').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-body').load(dataURL,function(){
            $('#modal-8').modal({show:true});
        });
    });     
});
</script>
<?php require_once('footer.php'); ?>
</div>
	
		
	</div>



<!-- Modal 8-->
<div class="modal fade custom-width in" id="modal-8">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content" style="max-height: 500px; overflow-x: hidden;">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><center>View All Restaurant Coupons</center></h4>
            </div>
            
            <div class="modal-body">
              
                 </div>
               

           

        </div>
    </div>
</div>


<!-- Modal 7 (Ajax Modal)-->
<div class="modal fade custom-width in" id="modal-7">
    <div class="modal-dialog" style="width:40%;">
        <div class="modal-content" style="max-height: 500px; overflow-x: hidden;">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><center>Add Foodomia Couppons</center></h4>
                
            </div>
            
            <div class="modal-body" id="addCoupan">
              
            </div>

        </div>
    </div>
</div>


<style>
.form-group.width {
    width: 100%;
}
</style>

	<?php require_once('footer_bottom.php');?>

</body>
</html>
