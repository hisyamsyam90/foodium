<div class="sidebar-menu" style="min-height: 100vh;">
  <header class="logo-env"> 
    
    <!-- logo -->
    <div class="logo"> <a href="" style="font-size: 35px;font-weight: bold;"> Foodium </div>
    
    <!-- logo collapse icon 
    <div class="sidebar-collapse"> 
        <a href="#" class="sidebar-collapse-icon with-animation">
      add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition 
      <i class="entypo-menu"></i> 
      </a> 
    </div>-->
    
    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
    <div class="sidebar-mobile-menu visible-xs"> <a href="#" class="with-animation"><!-- add class "with-animation" to support animation --> 
      <i class="entypo-menu"></i> </a> </div>
  </header>
  <ul id="main-menu" class="">
    <!-- add class "multiple-expanded" to allow multiple submenus to open --> 
    <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" --> 
    <!-- Search Bar -->

      <li> <a href="dashboard.php"><i class="entypo-home"></i> <span class="title">Dashboard</span> </a></li>

      <li style="background:rgba(69, 74, 84, 0.3);"> <a href="#" style="padding: 5px 10px;"><span class="title">Manage Menu</span></a> 
        <li> <a href="add-menu-details.php"><i class="entypo-plus-circled"></i> <span class="title">Add fast-food menu</span> </a></li>
        <li> <a href="add-category.php"><i class="entypo-plus-circled"></i> <span class="title">Add fast-food category</span></a></li>
      </li>

      <li style="background:rgba(69, 74, 84, 0.3);"> <a href="#" style="padding: 5px 10px;"><span class="title">Manage Delivery</span></a> 
         <li><a href=""><i class="entypo-clock"></i> <span class="title">Open/ Close Delivery</span></a> </li>
         <li><a href="viewOrderUpdates.php"><i class="entypo-monitor"></i> <span class="title">View Real-time Order</span></a> </li>
	    </li>

      <li style="background:rgba(69, 74, 84, 0.3);"> <a href="#" style="padding: 5px 10px;"><span class="title">Manage Sales</span></a> 
        <li> <a href=""><i class="entypo-doc-text"></i> <span class="title">View Sales</span> </a></li>
        <li> <a href="transactionHistory.php"><i class="entypo-clipboard"></i> <span class="title">Transaction History</span> </a></li>
        <li> <a href="add-location.php"><i class="entypo-plus-circled"></i> <span class="title">Add Location</span> </a></li>
      </li>

      <li style="background:rgba(69, 74, 84, 0.3);"> <a href="#" style="padding: 5px 10px;"><span class="title">Manage Runner</span></a> 
         <li> <a href=""><i class="entypo-users"></i> <span class="title">All Runners</span></a> </li>
         <li> <a href="../rider/index.php"><i class="entypo-plus-circled"></i> <span class="title">Register Runner</span></a> </li>
      
        <li style="background:rgba(69, 74, 84, 0.3);"> <a href="#" style="padding: 5px 10px;"><span class="title">Manage Commission</span></a> 
           <li><a href="add-commission.php"><i class="entypo-pencil"></i> <span class="title">Add Commission Level</span></a> </li>
           <li><a href="runner-commission.php"><i class="entypo-heart"></i> <span class="title">Runner Commission</span></a> </li>
      </li>

    <li style="background:rgba(69, 74, 84, 0.3);"> <a href="#" style="padding: 5px 10px;"><span class="title">Settings</span></a>
        <li> <a href=""><i class="entypo-users"></i> <span class="title">Admin Users</span></a> </li>
        <li> <a href="feedback.php"><i class="entypo-flag"></i> <span class="title">System Feedback</span></a> </li>
    </li>
  </ul>
</div>

