<?php require_once("Header.php"); ?>

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<?php require_once('left-sidebar.php'); ?>			
	<div class="main-content">
	<?php require_once('topbar.php'); ?>		
	
	<div class="row">
        <div class="col-md-12">
        
        <div class="pull-left"><h2 class="toast-title">Runner Commission</h2></div>
       <!-- <div class="pull-right"><a style="position: relative; top: 10px;" href='javascript:;' onClick='addtax()' class='btn btn-default'>Add Currency</a></div>-->
        <div class="clearfix"></div>
        <br>
         <table class="display nowrap table table-hover table-striped table-bordered" id="table-1">
            <thead>
                <tr>
                    <th><b>ID</b></th>
                    <th><b>Name<b></th>
                    <th><b>Date</b></th>
                    <th><b>Time</b></th>
                    <th><b>Amount(RM)</b></th>
					<th><b>Description</b></th>
					<th><b>Status</b></th>
                    <th><b>Action</b></th>
                </tr>
            </thead>
            <tbody>
                <td>FDM-R-17</td>
                <td>Mohd Mohsin bin Ismail</td>
                <td>31/06/2021</td>
                <td>12:32 PM</td>
                <td>45.00</td>
                <td>Commission June 2021</td>
                <td style="color:green;"><b>Paid</b></td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>Details</a>
            </tbody>
            <tbody>
                <td>FDM-R-08</td>
                <td>Muhammad Hazriq Akmal</td>
                <td>31/06/2021</td>
                <td>12:20 PM</td>
                <td>43.50</td>
                <td>Commission June 2021</td>
                <td style="color:red;"><b>Due</b></td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-success btn-sm'>Pay</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Cancel</a></td>
            </tbody>
            <tbody>
                <td>FDM-R-01</td>
                <td>Muhammad Fyruz Ismat</td>
                <td>31/06/2021</td>
                <td>12:14 PM</td>
                <td>22.90</td>
                <td>Commission June 2021</td>
                <td style="color:red;"><b>Due</b></td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-success btn-sm'>Pay</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Cancel</a></td>
            </tbody>
            <tbody>
                <td>FDM-R-12</td>
                <td>Ahmad Hisyam</td>
                <td>31/06/2021</td>
                <td>11:39 PM</td>
                <td>33.90</td>
                <td>Commission June 2021</td>
                <td style="color:green;"><b>Paid</b></td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-info btn-sm'>Details</a>
            </tbody>
            <tbody>
                <td>FDM-R-09</td>
                <td>Muhammad Rahimi</td>
                <td>31/06/2021</td>
                <td>11:13 PM</td>
                <td>55.65</td>
                <td>Commission June 2021</td>
                <td style="color:red;"><b>Due</b></td>
                <td><a href='javascript:;' class='viewrirestcoupons btn btn-success btn-sm'>Pay</a>
                <a href='javascript:;' class='viewrirestcoupons btn btn-danger btn-sm'>Cancel</a></td>
            </tbody>
        </table>
        
        <script type="text/javascript">
        
             <?php

                  if($_SESSION['role']=="0")
                  {
                     ?>
                          $('#table-1').DataTable({
                              dom: 'Bfrtip',
                              buttons: [
                                  'copy', 'csv', 'excel', 'pdf', 'print'
                              ]
                          });
                     <?php 
                  }
              ?>
        </script>
         
            
        </div>
    </div>    


<script type="text/javascript">
function addCoupan(data) {
   
   jQuery('#modal-7').modal('show', {backdrop: 'static'});
    document.getElementById("addCoupan").innerHTML="loading...";
    var xmlhttp;
    if(window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      xmlhttp.onreadystatechange=function()
      {
        if(xmlhttp.readyState==4 && xmlhttp.status==200)
        {
           // alert(xmlhttp.responseText);
            document.getElementById("addCoupan").innerHTML=xmlhttp.responseText;
        }
      }
    xmlhttp.open("GET","ajex-events.php?addCoupan=ok&resID="+data);
    xmlhttp.send();
	
}




jQuery(document).ready(function(){ 
	 $('.viewrirestcoupons').on('click',function(){
        var dataURL = $(this).attr('data-href');
        $('.modal-body').load(dataURL,function(){
            $('#modal-8').modal({show:true});
        });
    });     
});
</script>
<?php require_once('footer.php'); ?>
</div>
	
		
	</div>



<!-- Modal 8-->
<div class="modal fade custom-width in" id="modal-8">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content" style="max-height: 500px; overflow-x: hidden;">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><center>View All Restaurant Coupons</center></h4>
            </div>
            
            <div class="modal-body">
              
                 </div>
               

           

        </div>
    </div>
</div>


<!-- Modal 7 (Ajax Modal)-->
<div class="modal fade custom-width in" id="modal-7">
    <div class="modal-dialog" style="width:40%;">
        <div class="modal-content" style="max-height: 500px; overflow-x: hidden;">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><center>Add Foodomia Couppons</center></h4>
                
            </div>
            
            <div class="modal-body" id="addCoupan">
              
            </div>

        </div>
    </div>
</div>


<style>
.form-group.width {
    width: 100%;
}
</style>

	<?php require_once('footer_bottom.php');?>

</body>
</html>
